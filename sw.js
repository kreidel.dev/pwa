const CACHE_NAME = 'app-cache';
const PRE_CACHED_RESOURCES = [
    "/",
    "./sw-register.js",
    "./assets/css/styles.css",
    "./assets/js/app.js"
];


self.addEventListener('install', function (event) {
    async function preCacheResources() {
        const cache = await caches.open(CACHE_NAME);
        cache.addAll(PRE_CACHED_RESOURCES);
    }

    event.waitUntil(preCacheResources())
    self.skipWaiting();    
})

self.addEventListener('fetch', function (event) {
    async function returnCachedResource() {
        const cache = await caches.open(CACHE_NAME);
        const cachedResponse = await cache.match(event.request.url);

        if (cachedResponse) {
            return cachedResponse;
        } else {
            const fetchResponse = await fetch(event.request.url);
            cache.put(event.request.url, fetchResponse.clone());
            return fetchResponse
        }
    }

    event.respondWith(returnCachedResource());
})

self.addEventListener('activate', function (event) {
    async function deleteOldCaches() {
        const names = await caches.keys();
        await Promise.all(names.map(name => {
        if (name !== CACHE_NAME) {
            return caches.delete(name);
        }
    }));
  }

  event.waitUntil(deleteOldCaches());
})