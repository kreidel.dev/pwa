# Simple PWA HTML Template
[![Netlify Status](https://api.netlify.com/api/v1/badges/9eca1a0f-91ca-4500-8275-eea3c914cdf7/deploy-status)](https://app.netlify.com/sites/pwa-dev/deploys)

<a href="https://pwa-dev.netlify.app">Live demo</a>

## Getting started

1. [Use this template](https://gitlab.com/kreidel.dev/pwa)
2. Replace `icon-512.png` with your own
3. Run 
```
yarn install
yarn assets
yarn watch:scss
yarn serve
```
4. Enjoy ✨
